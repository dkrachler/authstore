import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BookingRow} from "../../models/booking-row";

/*
  Generated class for the BookingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BookingProvider {

  bookingRows: BookingRow[] = [];

  constructor(public http: HttpClient) {
    console.log('Hello BookingProvider Provider');
  }

  addBookingRow(bookingRow: any) {
    this.bookingRows.push(bookingRow);
    console.log(bookingRow);
  }

  removeBookingrow(id: string) {

  }

  editBookingRow(id: string){

  }

  getBookingRows(accountId: string) {
    return this.bookingRows;
  }

}
