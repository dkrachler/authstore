export class BookingRow {
  id: string;
  comment: string;
  accountId: string;
  amount: number;
  date: string;
  categories: number[];
}
