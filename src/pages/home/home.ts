import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {PersonProvider} from "../../providers/person/person";
import {Camera} from "@ionic-native/camera";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public personList: Array<any> = [];
  public personPic: string = null;

  constructor(public navCtrl: NavController,
              public personProvider: PersonProvider,
              public camera: Camera) {}

  ionViewDidLoad() {
    this.personProvider.getPersonList().on("value", personListSnapshot => {
      this.personList = [];
      personListSnapshot.forEach(snap => {
        this.personList.push({
          id: snap.key,
          name: snap.val().name,
          birthday: snap.val().birthday
        });
        return false;
      });
    });
  }

  // this function triggers the device image picker
  takePicture(): void {
    this.camera.getPicture({
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: this.camera.EncodingType.PNG,
      saveToPhotoAlbum: true
    }).then(
      imageData => {
        // response from image picker
        this.personPic = imageData;
      },
      error => {
        // error handling
        console.log('Error', JSON.stringify(error));
      }
    )
  }

  createPersonWithBirthday(
    personName: string,
    birthday: string,
  ): void {
    this.personProvider
      .createPerson(personName, birthday, this.personPic)
      .then(() => {
        // this.navCtrl.pop();
        this.personPic = null;
      });
  }

}
