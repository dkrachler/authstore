import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {NgForm} from "@angular/forms";
import {BookingRow} from "../../models/booking-row";
import {BookingProvider} from "../../providers/booking/booking";
import { Chart } from 'chart.js';

/**
 * Generated class for the FormsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forms',
  templateUrl: 'forms.html',
})
export class FormsPage {

  @ViewChild('barCanvas') barCanvas;
  barChart: any;

  bookingRow = BookingRow;
  bookingRows: BookingRow[] = [];
  balance: number;

  categories: any[] = [
    {id: 1, name: 'Haustiere'},
    {id: 2, name: 'Lebensmittel'},
    {id: 3, name: 'Kosmetik'},
    {id: 4, name: 'Körperpflege'},
    {id: 5, name: 'Auto'},
    {id: 6, name: 'Elektronik'},
  ];

  accounts: any[] = [
    {id: 1, name: 'Giro'},
    {id: 2, name: 'Cash'},
  ];

  constructor(private bookingService: BookingProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormsPage');
    this.drawChart();
  }

  drawChart(){
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Giro", "Cash"],
        datasets: [{
          label: 'Balance',
          data: [this.balance, 345],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:false
            }
          }]
        }
      }

    });
  }

  onSubmit(f: NgForm){
    this.bookingRow = f.value;
    console.log(this.bookingRows);
    this.bookingService.addBookingRow(this.bookingRow);
    this.bookingRows = this.bookingService.getBookingRows("1");
    this.calculateBalance();
    this.drawChart();
  }

  calculateBalance() {
    this.balance = 0;
    let bal = 0;
    this.bookingRows.forEach(row => {
      bal = bal + +row.amount;
    });
    this.balance = bal;
    return bal;
  }


}
